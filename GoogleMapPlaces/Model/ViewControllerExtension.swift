//
//  ViewControllerExtension.swift
//  Chatmate
//
//  Created by Pooja Rana on 23/05/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func setNavigationBarTitle() {
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18.0)]
    }
}
