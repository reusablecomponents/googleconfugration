GoogleMapPlaces works on Swift4.0 and requires ARC to build. It depends on the following Apple frameworks, which should already be included with most Xcode templates:

Foundation.framework
UIKit.framework

Alternatively you can directly add thePlacesModel.swift source file to your project.
The main guideline you need to follow when dealing with GoogleMapPlaces is:

Add
pod 'GooglePlaces'
pod 'GoogleMaps'

Use these lines in your code and get address, lat, long for map
self.openPlaces()
locationCallback = { strtAddress, location in
          let update = GMSCameraUpdate.setTarget(location, zoom: 15.0)
          self.mapContainerView.moveCamera(update)
}

