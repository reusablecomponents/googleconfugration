//
//  MainController.swift
//  GoogleMapPlaces
//
//  Created by Pooja Rana on 07/06/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit

class MainController: UIViewController {

    // MARK: -  ViewLifeCycle 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
}
