//
//  MarkerModel.swift
//  GoogleMapPlaces
//
//  Created by Pooja Rana on 07/06/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit

class MarkerModel: NSObject {
    var latitude: Double              = 0.0
    var longitude: Double             = 0.0
    var markerImg: UIImage?
    
    init(lat: Double, lng: Double, img: String) {
        latitude         = lat
        longitude        = lng
        markerImg        = UIImage.init(named: img)
    }
}
