//
//  CustomMapController.swift
//  GoogleMapPlaces
//
//  Created by Pooja Rana on 07/06/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit
import GoogleMaps

class CustomMapController: UIViewController {

    @IBOutlet weak var mapContainerView: GMSMapView!
    var marketModelArr = [MarkerModel]()
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setNavigationBarTitle()
        mapContainerView.mapType = .normal
        
        //Sector 17 Location
        marketModelArr.append(MarkerModel(lat: 30.7398, lng:76.7827, img: "dummyImg1"))
        //Sector 18 Location
        marketModelArr.append(MarkerModel(lat: 30.7332, lng:76.7870, img: "dummyImg2"))
        //Sector 19 Location
        marketModelArr.append(MarkerModel(lat: 30.7282, lng:76.7932, img: "dummyImg3"))
        //Sector 22 Location
        marketModelArr.append(MarkerModel(lat: 30.72811, lng: 76.77065, img: "dummyImg4"))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        setCameraAndAddMarkersOnMap()
    }
    
    // MARK: -  IBOutlet Action 
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: -  Google Maps 
    //Initiate and load map
    func setCameraAndAddMarkersOnMap () {
        let marker = marketModelArr.first
        let camera = GMSCameraPosition.camera(withLatitude: (marker?.latitude)!, longitude: (marker?.longitude)!, zoom: 15.0)
        mapContainerView.camera = camera
        mapContainerView.isMyLocationEnabled = false
        

        //Add markers on map and Move camera to show complete region over map
        var bounds = GMSCoordinateBounds()
        for markerModel in marketModelArr {
            
            self.addMarkersOnLocation(model: markerModel)
            bounds = bounds.includingCoordinate(CLLocationCoordinate2DMake(markerModel.latitude, markerModel.longitude))
        }
        let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
        mapContainerView.animate(with: update)
    }
    
    // Set and Focus Marker
    func addMarkersOnLocation (model: MarkerModel) {
        let currentMarker = GMSMarker.init(position: CLLocationCoordinate2DMake(model.latitude, model.longitude))
        currentMarker.map               =   mapContainerView
        currentMarker.icon              =   model.markerImg
        currentMarker.appearAnimation   =   .pop
    }
}
