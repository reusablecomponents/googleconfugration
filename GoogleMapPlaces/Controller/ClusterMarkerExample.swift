//
//  ClusterMarkerExample.swift
//  GoogleMapPlaces
//
//  Created by Pooja Rana on 07/09/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit
import GoogleMaps

class ClusterMarkerExample: UIViewController, GMUClusterManagerDelegate, GMSMapViewDelegate, GMUClusterRendererDelegate {

    @IBOutlet weak var mapContainerView: GMSMapView!
    private var clusterManager: GMUClusterManager!
    var markerArr = [POIItem]()

    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavigationBarTitle()
        mapContainerView.mapType = .normal
        
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapContainerView,
                                                 clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        clusterManager = GMUClusterManager(map: mapContainerView, algorithm: algorithm,
                                           renderer: renderer)
        
        // Generate and add random items to the cluster manager.
        generateClusterItems()
        
        // Call cluster() after items have been added to perform the clustering
        // and rendering on map.
        clusterManager.cluster()
        
        // Register self to listen to both GMUClusterManagerDelegate and
        // GMSMapViewDelegate events.
        clusterManager.setDelegate(self, mapDelegate: self)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        setCameraOnMap()
    }
    
    /// Randomly generates cluster items within some extent of the camera and
    /// adds them to the cluster manager.
    private func generateClusterItems() {
        
        //Sector 17 Location
        let item1 = POIItem(position: CLLocationCoordinate2DMake(30.7398, 76.7827), name: "Sector 17", image: "dummyImg1")
        markerArr.append(item1)
        
        //Sector 18 Location
        let item2 = POIItem(position: CLLocationCoordinate2DMake(30.7332, 76.7870), name: "Sector 18", image: "dummyImg2")
        markerArr.append(item2)
        
        //Sector 19 Location
        let item3 = POIItem(position: CLLocationCoordinate2DMake(30.7282, 76.7932), name: "Sector 19", image: "dummyImg3")
        markerArr.append(item3)
        
        //Sector 22 Location
        let item4 =
            POIItem(position: CLLocationCoordinate2DMake(30.72811, 76.77065), name: "Sector 22", image: "dummyImg4")
        markerArr.append(item4)
        
        clusterManager.add(markerArr)
    }
    
    // MARK: -  IBOutlet Action 
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: -  Google Maps 
    
    //Initiate and load map
    func setCameraOnMap () {
        let marker = markerArr.first
        let camera = GMSCameraPosition.camera(withLatitude: (marker?.position.latitude)!, longitude: (marker?.position.longitude)!, zoom: 15.0)
        mapContainerView.camera = camera
        mapContainerView.isMyLocationEnabled = false


        //Add markers on map and Move camera to show complete region over map
        var bounds = GMSCoordinateBounds()
        for marker in markerArr {
            bounds = bounds.includingCoordinate(marker.position)
        }
        
        let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
        mapContainerView.animate(with: update)
    }
    
    // MARK: - GMUClusterManagerDelegate
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,
                                                           zoom: mapContainerView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        mapContainerView.moveCamera(update)
        return true
    }
   
    
    // MARK: - GMUMapViewDelegate
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let poiItem = marker.userData as? POIItem {
            NSLog("Did tap marker for cluster item \(poiItem.name)")
        } else {
            NSLog("Did tap a normal marker")
        }
        return false
    }
   
    func renderer(_ renderer: GMUClusterRenderer, markerFor object: Any) -> GMSMarker? {
        
        let marker = GMSMarker()
        if object is POIItem {
            // set image view for gmsmarker
            let item = object as? POIItem
            marker.title = item?.name
            marker.icon = UIImage.init(named: (item?.image)!)
            return marker
        }
        return nil
    }
}

class POIItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var name: String!
    var image: String
    
    init(position: CLLocationCoordinate2D, name: String, image: String) {
        self.position = position
        self.name = name
        self.image = image
    }
}
