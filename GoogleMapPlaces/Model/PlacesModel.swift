//
//  PlacesModel.swift
//  GoogleMapPlaces
//
//  Created by Pooja Rana on 19/03/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import Foundation
import GooglePlaces
import UIKit

var locationCallback : ((_ strtAddress: String, _ location: CLLocationCoordinate2D) ->())?

extension UIViewController: GMSAutocompleteViewControllerDelegate {
    
    func openPlaces() {
        let autocompleteController      = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        //If want to apply country based filter
//        let filter                      = GMSAutocompleteFilter()
//        filter.country                  = "AU"
//        autocompleteController.autocompleteFilter = filter
        present(autocompleteController, animated: true, completion: nil)
    }
    
    // Handle the user's selection.
    public func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        locationCallback!(place.formattedAddress!, place.coordinate)
        dismiss(animated: true, completion: nil)
    }
    
    public func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    public func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    public func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    public func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
