//
//  PlaceMapController.swift
//  GoogleMapPlaces
//
//  Created by Pooja Rana on 19/03/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit
import GoogleMaps

class PlaceMapController: UIViewController, GMSMapViewDelegate {

    // MARK: -  IBOutlet 
    @IBOutlet weak var strtAddress   : UILabel!
    @IBOutlet weak var mapContainerView: GMSMapView!
    
    var currentMarker       : GMSMarker?
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setNavigationBarTitle()
        mapContainerView.delegate =  self
        mapContainerView.mapType  =  .normal
        self.setcurrentMarkerPosition()
        let location = CLLocation.init(latitude: -33.865143, longitude: 151.209900)
        let camera = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: 2.0)
        mapContainerView.animate(to: camera)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    // MARK: -  IBOutlet Action 
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addressAction(_ sender: Any) {
        self.openPlaces()
        locationCallback = { strtAddress, location in
            self.strtAddress.text = strtAddress
            let update = GMSCameraUpdate.setTarget(location, zoom: 15.0)
            self.mapContainerView.moveCamera(update)
            
            self.mapContainerView.clear()
            self.setcurrentMarkerPosition()
            self.currentMarker?.position = location
        }
    }
    
    // MARK: -  LocationManager Delegates 
    func setcurrentMarkerPosition() {
        currentMarker                   =   GMSMarker()
        currentMarker?.map              =   mapContainerView
        currentMarker?.icon             =   UIImage.init(named: "location")
        currentMarker?.appearAnimation  =   GMSMarkerAnimation.pop
    }
}
